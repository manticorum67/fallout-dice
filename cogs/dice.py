import re
import logging

import discord
from discord.ext import commands, tasks
import random


async def get_emoji(ctx, name, return_empty=False):
    try:
        emoji = await commands.converter.EmojiConverter().convert(ctx, name)
    except:
        if return_empty:
            emoji = ''
        else:
            return name
    return emoji


class Dice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def cog_command_error(self, ctx, error):
        await ctx.send(f'{error}')

    @commands.command(name='1d20', aliases=['d20'], help='1d20 or d20', hidden=True)
    async def dtwenty_roll(self, ctx):
        """
        Roll a single d20.
        """
        d_twenty = random.randint(1, 20)

        roll_message = f'd20 roll for {ctx.author.name}\n```md\n# {str(d_twenty)}\n'\
                       f'Details:[1d20 ({d_twenty})]```'

        await ctx.channel.send(roll_message)

    @commands.command(name='roll', aliases=['r'], help='!r XtY; X = #d20s, Y = target')
    async def dice_pool_roll(self, ctx, dice_string):
        if not re.search('[0-9]+t[0-9]+', dice_string.lower()):
            await ctx.send('Please format the dice as **X**t**Y** and try again.')
            return

        numbers = re.split('t', dice_string.lower())
        logging.info(f'numbers: {numbers}')
        num_dice = int(numbers[0])
        target = int(numbers[1])
        successes = 0
        complications = 0
        all_rolls = []
        embed = discord.Embed(title=f'{num_dice}d{20} roll for {ctx.author.name}\n')
        embed.set_footer(text='If this is a Tag Skill, make sure to check for non-1 critical successes')

        logging.info(f'num_dice: {num_dice} / target: {target}')

        if num_dice < 1 or target < 1:
            await ctx.send('Hurr hurr, let\'s roll negative dice, amirite!')
            return

        if num_dice > 10:
            await ctx.send('Ain\'t nobody got time for dat!')
            return

        for x in range(num_dice):
            this_roll = random.randint(1, 20)
            stars = '**'
            if this_roll == 1:
                successes += 2
            elif this_roll <= target:
                successes += 1
            else:
                stars = ''
                if this_roll == 20:
                    complications += 1

            all_rolls.append(f'{stars}{this_roll}{stars}')

        embed.add_field(name='All Dice', value=f'{", ".join(all_rolls)}', inline=False)
        embed.add_field(name='Target', value=f'{target}')
        embed.add_field(name='Successes', value=f'{successes}')
        if complications > 0:
            embed.add_field(name='Complications', value=f'{complications}')

        await ctx.send(content=None, embed=embed)

    @commands.command(name='combat', aliases=['d', 'dmg', 'damage'], help='!dmg X; X = #d6s')
    async def damage_roll(self, ctx, num_dice: int):
        if num_dice > 20:
            await ctx.send('Ain\'t nobody got time for dat!')
            return

        dice_string = ''
        damage = 0
        effects = 0
        embed = discord.Embed(title=f'Combat Dice roll for {ctx.author.name}\n')

        for x in range(num_dice):
            this_roll = random.randint(1, 6)
            if len(dice_string) > 0:
                dice_string += f', '

            if this_roll == 1:
                dice_string += f'{await get_emoji(ctx, "onedmg")}'
                damage += 1
            elif this_roll == 2:
                dice_string += f'{await get_emoji(ctx, "twodmg")}'
                damage += 2
            elif this_roll <= 4:
                dice_string += f'{await get_emoji(ctx, "blank")}'
            else:
                dice_string += f'{await get_emoji(ctx, "onedmgpluseffect")}'
                damage += 1
                effects += 1

        embed.add_field(name='All Dice', value=f'{dice_string}', inline=False)
        embed.add_field(name='Damage', value=f'{damage}')
        embed.add_field(name='Effects', value=f'{effects}')

        await ctx.send(content=None, embed=embed)


def setup(bot):
    bot.add_cog(Dice(bot))
