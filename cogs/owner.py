import logging
from discord.ext import commands


class Owner(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True)
    @commands.is_owner()
    async def load(self, ctx, *, cog: str):
        try:
            self.bot.load_extension(f'cogs.{cog}')
            logging.warning(f'Loaded {cog}')
        except Exception as e:
            await ctx.send(f'**ERROR:** {type(e).__name__} - {e}')
        else:
            await ctx.send('**SUCCESS**')

    @commands.command(hidden=True)
    @commands.is_owner()
    async def unload(self, ctx, *, cog: str):
        try:
            self.bot.unload_extension(f'cogs.{cog}')
            logging.warning(f'Unloaded {cog}')
        except Exception as e:
            await ctx.send(f'**ERROR:** {type(e).__name__} - {e}')
        else:
            await ctx.send('**SUCCESS**')

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload(self, ctx, *, cog: str):
        try:
            self.bot.unload_extension(f'cogs.{cog}')
            logging.warning(f'Unloaded {cog}')
            self.bot.load_extension(f'cogs.{cog}')
            logging.warning(f'Reloaded {cog}')
        except Exception as e:
            await ctx.send(f'**ERROR:** {type(e).__name__} - {e}')
        else:
            await ctx.send('**SUCCESS**')

    @commands.command(hidden=True)
    @commands.is_owner()
    async def fullreset(self, ctx):
        cogs = []

        for x in cogs:
            try:
                self.bot.unload_extension(f'cogs.{x}')
                logging.warning(f'Unloaded {x}')
            except Exception as e:
                await ctx.send(f'Failed to unload **{x}**')

        for x in cogs:
            try:
                self.bot.load_extension(f'cogs.{x}')
                logging.warning(f'Loaded {x}')
            except Exception as e:
                await ctx.send(f'Failed to load **{x}**')

        await ctx.send('**SUCCESS**')


def setup(bot):
    bot.add_cog(Owner(bot))
