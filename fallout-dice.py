import datetime
import logging
import os

import discord
from discord.ext import commands

date = f'{datetime.datetime.now().year}-{datetime.datetime.now().month}-{datetime.datetime.now().day}'
log_level = logging.INFO if os.environ.get('LOG_LEVEL') == 'INFO' else 'WARN'
logging.basicConfig(
    filename=f'logs/{date}.log',
    format='%(asctime)s - fallout-dice - %(levelname)s - %(message)s',
    level=log_level
)

COGS = [
    'cogs.owner',
    'cogs.dice',
]

intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix='!',
                   intents=intents,
                   description='The Fallout Dice Bot\nIf you have questions, feel free to contact Cal.',
                   case_insensitive=True,
                   owner_id=258104532423147520)


@bot.event
async def on_ready():
    logging.info('Logged in as:')
    logging.info(bot.user.name)
    logging.info(bot.user.id)


for c in COGS:
    try:
        bot.load_extension(c)
        logging.info(f'Loaded cog: {c}')
    except Exception as e:
        logging.error(f'Failed to load cog: {c}')
        logging.error(f'{e}')

bot.run(os.environ.get('BOT_TOKEN'))
